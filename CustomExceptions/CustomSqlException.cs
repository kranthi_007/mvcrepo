﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomExceptions
{
    public class CustomSqlException:Exception
    {
        public CustomSqlException(string message, Exception exception) : base(message, exception)
        {

        }
    }
}
