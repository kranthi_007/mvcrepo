﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomExceptions
{
    public class DataNotInsertedException:Exception
    {
        public DataNotInsertedException(string Message, Exception exception) : base(Message, exception)
        {

        }
        public DataNotInsertedException(string Message) : base(Message)
        {

        }
    }
}
