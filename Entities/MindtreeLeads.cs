﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class MindtreeLeads
    {
        public int LeadId { get; set; }
        public string LeadName { get; set; }
        public string ContactNumber { get; set; }
        public int CategoryId { get; set; }
    }
}
