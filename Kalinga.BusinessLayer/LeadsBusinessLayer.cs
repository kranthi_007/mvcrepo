﻿using Entities;
using Kalinga.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalinga.BusinessLayer
{
    public class LeadsBusinessLayer
    {
        readonly MindtreeLeadsDal leadDal = new MindtreeLeadsDal();
        public int AddLeadDetails(MindtreeLeads mindtreeLead, Category category)
        {
            return leadDal.AddAllLeadsDetails(mindtreeLead, category);
        }
        public List<MindtreeLeads> DisplayDetailsOfLead()
        {
            return leadDal.DisplayAllDetailsOfLeads();
        }
        public List<Category> GetDetailsOfCategory()
        {
            return leadDal.GetAllDetailsOfCategory();
        }
        public int UpdateLeadDetail(MindtreeLeads mindtreeLead, Category category)
        {
            return leadDal.UpdateDetailsOfMindtreeLeads(mindtreeLead, category);
        }

    }
}
