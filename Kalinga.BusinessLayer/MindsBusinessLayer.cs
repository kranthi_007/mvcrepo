﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using CustomExceptions;
using Kalinga.DataAccessLayer;

namespace Kalinga.BusinessLayer
{
    public class MindsBusinessLayer
    {
        readonly CampusMindsDal campusMindsDal = new CampusMindsDal();
        public int AddCampusMindsDetails(CampusMinds campusMind,MindtreeLeads mindtreeLeads)
        {
            return campusMindsDal.InsertDataOfMinds(campusMind,mindtreeLeads);
        }
        public List<CampusMinds> GetCampusMindsDetails()
        {
            return campusMindsDal.GetDetailsOfCampusMinds();
        }
        public List<CampusMinds> DisplayCampusMindDetails()
        {
            return campusMindsDal.DisplayCampusMindsDetails();
        }
        public int UpdateMindDetail(CampusMinds campusMind, MindtreeLeads lead)
        {
            return campusMindsDal.UpdateDetailsOfCampusMinds(campusMind, lead);
        }
    }
}
