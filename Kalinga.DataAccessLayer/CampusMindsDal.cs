﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using CustomExceptions;
using Microsoft.Build.Framework.XamlTypes;

namespace Kalinga.DataAccessLayer
{
    public class CampusMindsDal
    {
        readonly ConnectionToDataBase connection = new ConnectionToDataBase();
        public int InsertDataOfMinds(CampusMinds campusMinds, MindtreeLeads mindtreeLeads)
        {
            int rowAffect=0;
            try
            {
                connection.OpenConnection();
                SqlCommand command = new SqlCommand("InsertCampusMind", connection.GetConnection());
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter;

                parameter = command.Parameters.Add("@MindName", SqlDbType.VarChar, 50);
                parameter.Value = campusMinds.MindName;

                parameter = command.Parameters.Add("@DOJ", SqlDbType.DateTime);
                parameter.Value = campusMinds.DOJ;

                parameter = command.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 50);
                parameter.Value = campusMinds.ContactNumber;

                parameter = command.Parameters.Add("@Address", SqlDbType.VarChar, 50);
                parameter.Value = campusMinds.Address;

                parameter = command.Parameters.Add("@LeadId", SqlDbType.Int);
                parameter.Value = mindtreeLeads.LeadId;

                rowAffect = command.ExecuteNonQuery();

                if (rowAffect > 0)
                {
                    return rowAffect;
                }
                else
                {
                    throw new DataNotInsertedException("data is not inserted");
                }
            }
            catch (DataNotInsertedException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                connection.CloseConnection();
            }
            return rowAffect;
        }

        public List<CampusMinds> GetDetailsOfCampusMinds()
        {
            List<CampusMinds> campusMinds = new List<CampusMinds>();

            try
            {
               connection.OpenConnection();

                SqlCommand cmd = new SqlCommand("select * from GetMindDetails()", connection.GetConnection());

                SqlDataReader sqlDataReader = cmd.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    CampusMinds campusMind = new CampusMinds()
                    {
                        MindId = sqlDataReader.GetInt32(0),
                        MindName = sqlDataReader.GetString(1),
                        DOJ = sqlDataReader.GetDateTime(2),
                        ContactNumber = sqlDataReader.GetString(3),
                        Address = sqlDataReader.GetString(4),
                        LeadId = sqlDataReader.GetInt32(5)
                    };

                    campusMinds.Add(campusMind);
                }
            }
            catch (SqlException ex)
            {
                throw new DataNotInsertedException("data is not inserted",ex);
                //Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.CloseConnection();
            }
            return campusMinds;

        }
        public List<CampusMinds> DisplayCampusMindsDetails()
        {
            List<CampusMinds> campusMind = new List<CampusMinds>();

            try
            {
                connection.OpenConnection();
                SqlCommand cmd = new SqlCommand("GetCampusMindsDetails", connection.GetConnection());

                cmd.CommandType = CommandType.StoredProcedure;
                var mindDetails = cmd.ExecuteReader();

                while (mindDetails.Read())
                {
                    CampusMinds mind = new CampusMinds()
                    {
                        MindId = mindDetails.GetInt32(0),
                        MindName = mindDetails.GetString(1),
                        DOJ = mindDetails.GetDateTime(2),
                        ContactNumber = mindDetails.GetString(3),
                        Address = mindDetails.GetString(4),
                        LeadId = mindDetails.GetInt32(5)
                    };
                    campusMind.Add(mind);
                }
                mindDetails.Close();
            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("no data to dispaly", ex);
               //Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.CloseConnection();
            }
            return campusMind;
        }
        public int UpdateDetailsOfCampusMinds(CampusMinds campusMind, MindtreeLeads lead)
        {
            int rowAffected = 0;
            try
            {
                connection.OpenConnection();
                SqlCommand cmd = new SqlCommand("UpdateCampusMindsDetails", connection.GetConnection())
                {
                    CommandType = CommandType.StoredProcedure
                };

                SqlParameter sqlParameter;

                sqlParameter = cmd.Parameters.Add("@MindId", SqlDbType.Int);
                sqlParameter.Value = campusMind.MindId;

                sqlParameter = cmd.Parameters.Add("@MindName", SqlDbType.VarChar, 50);
                sqlParameter.Value = campusMind.MindName;

                sqlParameter = cmd.Parameters.Add("@DOJ", SqlDbType.DateTime);
                sqlParameter.Value = campusMind.DOJ;

                sqlParameter = cmd.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 50);
                sqlParameter.Value = campusMind.ContactNumber;

                sqlParameter = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 50);
                sqlParameter.Value = campusMind.Address;

                sqlParameter = cmd.Parameters.Add("@LeadId", SqlDbType.Int);
                sqlParameter.Value = lead.LeadId;

                rowAffected = cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("no data to dispaly", ex);
                // Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.CloseConnection();
            }
            return rowAffected;
        }
    }
}
