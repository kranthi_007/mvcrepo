﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalinga.DataAccessLayer
{
    public class ConnectionToDataBase
    {
       private static readonly string conString = ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;
        private readonly SqlConnection con = new SqlConnection(conString);
        public SqlConnection GetConnection()
        {
            return con;
        }
        public void OpenConnection()
        {
            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public void CloseConnection()
        {
            if (con.State == System.Data.ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}
