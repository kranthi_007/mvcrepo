﻿using CustomExceptions;
using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalinga.DataAccessLayer
{
    public class MindtreeLeadsDal
    {
        readonly ConnectionToDataBase connection = new ConnectionToDataBase();

        public int AddAllLeadsDetails(MindtreeLeads mindtreeLead, Category category)
        {
            int rowsAffect = 0;
            try
            {
                connection.OpenConnection();

                SqlCommand cmd = new SqlCommand("InsertDetailsOfLeads", connection.GetConnection());
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlParameter;

                //sqlParameter = cmd.Parameters.Add("@LeadId", SqlDbType.Int, 10);
                //sqlParameter.Value = mindtreeLead.LeadId;

                sqlParameter = cmd.Parameters.Add("@LeadName", SqlDbType.VarChar, 50);
                sqlParameter.Value = mindtreeLead.LeadName;

                sqlParameter = cmd.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 50);
                sqlParameter.Value = mindtreeLead.ContactNumber;

                sqlParameter = cmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                sqlParameter.Value = category.CategoryId;

                rowsAffect =cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("data is not inserted",ex);
                //Console.WriteLine(ex.Message);
                //throw ex;
            }
            finally
            {
                connection.CloseConnection();
            }
            return rowsAffect;
        }
        public List<Category> GetAllDetailsOfCategory()
        {
            List<Category> categories = new List<Category>();

            try
            {
                connection.OpenConnection();

                SqlCommand cmd = new SqlCommand("GetCategoryDetails", connection.GetConnection());

                SqlDataReader sqlDataReader = cmd.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    Category category = new Category()
                    {
                        CategoryId = sqlDataReader.GetInt32(0),
                        CategoryName = sqlDataReader.GetString(1)
                    };
                    categories.Add(category);
                }
            }
            catch (SqlException ex)
            {
                throw new DataNotInsertedException("Data not found", ex);
                //Console.WriteLine(ex.Message);
            }
            finally
            {
               connection.CloseConnection();
            }
            return categories;
        }

        public List<MindtreeLeads> DisplayAllDetailsOfLeads()
        {
            List<MindtreeLeads> mindtreeLeads = new List<MindtreeLeads>();

            try
            {
                connection.OpenConnection();
                SqlCommand cmd = new SqlCommand("select * from MindtreeLeads", connection.GetConnection());
                //cmd.CommandType = CommandType.StoredProcedure;
                var leadDetails = cmd.ExecuteReader();

                while (leadDetails.Read())
                {
                    MindtreeLeads mindtreeLead = new MindtreeLeads()
                    {
                        LeadId = leadDetails.GetInt32(0),
                        LeadName = leadDetails.GetString(1),
                        ContactNumber = leadDetails.GetString(2),
                        CategoryId = leadDetails.GetInt32(3)
                    };
                    mindtreeLeads.Add(mindtreeLead);
                }
                leadDetails.Close();
            }
            catch (SqlException ex)
            {
               throw new CustomSqlException("data not found", ex);
               //Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.CloseConnection();
            }
            return mindtreeLeads;
        }

        public int UpdateDetailsOfMindtreeLeads(MindtreeLeads mindtreeLead, Category category)
        {
            int rowAffected=0;
            try
            {
                connection.OpenConnection();
                SqlCommand cmd = new SqlCommand("UpdateLeadDetails", connection.GetConnection());

                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter para;

                para = cmd.Parameters.Add("@LeadId", SqlDbType.Int);
                para.Value = mindtreeLead.LeadId;

                para = cmd.Parameters.Add("@LeadName", SqlDbType.VarChar, 50);
                para.Value = mindtreeLead.LeadName;

                para = cmd.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 50);
                para.Value = mindtreeLead.ContactNumber;

                //para = cmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                //para.Value = category.CategoryId;

                rowAffected = cmd.ExecuteNonQuery();
                //connection.CloseConnection();
            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Data not updated..", ex);
               //Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.CloseConnection();
            }
            return rowAffected;
        }
    }
}
