﻿using CustomExceptions;
using Kalinga.PresentationLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kalinga.PresentationLayer.Controllers
{
    public class CampusMindsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        readonly ManagerModel managerModel = new ManagerModel();

        [HttpGet]
        public ActionResult GetCampusMinds()
        {
            var context = managerModel.DisplayDetailsOfLead();
            if (context != null)
            {
                ViewBag.data = context.ToList();
            }
            return View();
        }

        [HttpPost]
        public ActionResult GetCampusMinds(CampusMindsModel campusMindModel, MindtreeLeadsModel mindtreeLeadModel)
        {
            try
            {
                int rowAffected = managerModel.AddCampusMindsDetails(campusMindModel, mindtreeLeadModel);

                var context = managerModel.DisplayDetailsOfLead();
                if (context != null)
                {
                    ViewBag.data = context.ToList();
                }
                if (rowAffected > 0)
                {
                    ViewBag.message = "Data inserted successfully";
                }
                else
                {
                    ViewBag.message = "Data not inserted";
                }
            }
            catch (CustomSqlException ex)
            {
                ViewBag.message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View();
        }
        [HttpGet]

        public ActionResult DisplayCampusMinds()
        {
            List<CampusMindsModel> campusMindModels = new List<CampusMindsModel>();

            campusMindModels = managerModel.DisplayCampusMindDetails();
            return View(campusMindModels);
        }

        [HttpGet]
        public ActionResult UpdateMindDetails()
        {
            int id = 0;
            var context = managerModel.DisplayDetailsOfLead();

            if (context != null)
            {
                ViewBag.data = context.ToList();
            }
            return View(managerModel.DisplayCampusMindDetails().Find(Val => Val.MindId==id));
        }

        [HttpPost]
        public ActionResult UpdateMindDetails(CampusMindsModel campusMindModel, MindtreeLeadsModel mindtreeLeadModel)
        {
            try
            {
                int rowAffected = managerModel.UpdateMindDetail(campusMindModel, mindtreeLeadModel);

                ModelState.Clear();
                var context = managerModel.DisplayDetailsOfLead();
                if (context != null)
                {
                    ViewBag.data = context.ToList();
                }
                if (rowAffected > 0)
                {
                    ViewBag.message = "Updated Successfully";
                }
                else
                {
                    ViewBag.message = "Not updated..";
                }
            }
            catch (CustomSqlException ex)
            {
                ViewBag.message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult GetLeadsDetails()
        {
            var context = managerModel.GetDetailsOfCategory();
            if (context != null)
            {
                ViewBag.data = context.ToList();
            }
            return View();
        }

        [HttpPost]
        public ActionResult GetLeadsDetails(MindtreeLeadsModel mindtreeLeadModel, CategoryModel categoryModel)
        {
            try
            {
                int rowAffected = managerModel.AddLeadDetails(mindtreeLeadModel, categoryModel);

                var context = managerModel.GetDetailsOfCategory();
                if (context != null)
                {
                    ViewBag.data = context.ToList();
                }
                if (rowAffected > 0)
                {
                    ViewBag.message = "Data inserted successfully";
                }
                else
                {
                    ViewBag.message = "Data not inserted";
                }
            }
            catch (CustomSqlException ex)
            {
                ViewBag.message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult DisplayLeadDetails()
        {
            List<MindtreeLeadsModel> mindtreeLeadModels = new List<MindtreeLeadsModel>();

            mindtreeLeadModels = managerModel.DisplayDetailsOfLead();

            return View(mindtreeLeadModels);
        }

        [HttpGet]
        public ActionResult UpdateLeads()
        {
            int id = 0;
            var context = managerModel.GetDetailsOfCategory();
            if (context != null)
            {
                ViewBag.data = context.ToList();
            }
            return View(managerModel.DisplayDetailsOfLead().Find(Val => Val.LeadId == id));
        }

        [HttpPost]
        public ActionResult UpdateLeads(MindtreeLeadsModel mindtreeLeadModel, CategoryModel categoryModel)
        {
            try
            {
                int row = managerModel.UpdateLeadDetail(mindtreeLeadModel, categoryModel);
                ModelState.Clear();
                var context = managerModel.GetDetailsOfCategory();
                if (context != null)
                {
                    ViewBag.data = context.ToList();
                }
                if (row > 0)
                {
                    ViewBag.message = "Updated successfully";
                }
                else
                {
                    ViewBag.message = "not updated";
                }
                return View();
            }
            catch (CustomSqlException ex)
            {
                ViewBag.message = ex.Message;
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            return View("Details");
        }
    }
}