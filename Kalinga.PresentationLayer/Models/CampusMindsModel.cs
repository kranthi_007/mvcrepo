﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kalinga.PresentationLayer.Models
{
    public class CampusMindsModel
    {
        [DisplayName("Mind Id")]
        public int MindId { get; set; }

        [DisplayName("Mind Name")]
        [Required(ErrorMessage = "Enter the Mind Name")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Invalid Mind Name")]
        public string MindName { get; set; }

        [DisplayName("Date Of Joining")]
        [Required(ErrorMessage = "Enter the Date Of Joining")]
        public DateTime DOJ { get; set; }

        [DisplayName("Contact Number")]
        [Required(ErrorMessage = "Enter the Contact Number")]
        [RegularExpression("^[0-9]{10}$", ErrorMessage = "Invalid Contact Number")]
        public string ContactNumber { get; set; }

        [DisplayName("Mind Address")]
        [Required(ErrorMessage = "Enter the  Mind Address")]
        public string Address { get; set; }

        [DisplayName("Lead Id")]
        [Required(ErrorMessage = "Enter the LeadId")]
        public int LeadId { get; set; }
    }
}