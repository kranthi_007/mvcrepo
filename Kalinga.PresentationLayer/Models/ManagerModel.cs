﻿using Entities;
using Kalinga.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Kalinga.PresentationLayer.Models
{
    public class ManagerModel
    {
        readonly LeadsBusinessLayer leadBL = new LeadsBusinessLayer();
        readonly MindsBusinessLayer mindsBL = new MindsBusinessLayer();
        public int AddCampusMindsDetails(CampusMindsModel campusMindModel, MindtreeLeadsModel mindtreeLeadModel)
        {
            CampusMinds campusMind = new CampusMinds
            {
                MindId = campusMindModel.MindId,
                MindName = campusMindModel.MindName,
                DOJ = campusMindModel.DOJ,
                ContactNumber = campusMindModel.ContactNumber,
                Address = campusMindModel.Address
            };
            MindtreeLeads mindtreeLead = new MindtreeLeads
            {
                LeadId = mindtreeLeadModel.LeadId
            };
            return mindsBL.AddCampusMindsDetails(campusMind,mindtreeLead);
        }
        public List<CampusMindsModel> DisplayCampusMindDetails()
        {

            List<CampusMindsModel> campusMindModels = new List<CampusMindsModel>();
            List<CampusMinds> campusMinds = mindsBL.DisplayCampusMindDetails();

            foreach (var mind in campusMinds)
            {
                campusMindModels.Add(AssignMind(mind));
            }
            return campusMindModels;
        }
        public CampusMindsModel AssignMind(CampusMinds mind)
        {
            CampusMindsModel campusMindModel = new CampusMindsModel()
            {
                MindId = mind.MindId,
                MindName = mind.MindName,
                DOJ = mind.DOJ,
                ContactNumber = mind.ContactNumber,
                Address = mind.Address,
                LeadId = mind.LeadId
            };
            return campusMindModel;
        }
        public int UpdateMindDetail(CampusMindsModel campusMindModel, MindtreeLeadsModel mindtreeLeadModel)
        {
            CampusMinds campusMind = new CampusMinds()
            {
                MindId = campusMindModel.MindId,
                MindName = campusMindModel.MindName,
                DOJ = campusMindModel.DOJ,
                ContactNumber = campusMindModel.ContactNumber,
                Address = campusMindModel.Address
            };
            MindtreeLeads mindtreeLead = new MindtreeLeads()
            {
                LeadId = mindtreeLeadModel.LeadId
            };
            return mindsBL.UpdateMindDetail(campusMind, mindtreeLead);
        }

        public int AddLeadDetails(MindtreeLeadsModel mindtreeLeadModel, CategoryModel categoryModel)
        {
            MindtreeLeads mindtreeLead = new MindtreeLeads
            {
                LeadId = mindtreeLeadModel.LeadId,
                LeadName = mindtreeLeadModel.LeadName,
                ContactNumber = mindtreeLeadModel.ContactNumber
            };
            Category category = new Category
            {
                CategoryId = categoryModel.CategoryId,
            };
            return leadBL.AddLeadDetails(mindtreeLead, category);
        }

        public List<MindtreeLeadsModel> DisplayDetailsOfLead()
        {
            List<MindtreeLeadsModel> mindtreeLeadModels = new List<MindtreeLeadsModel>();

            List<MindtreeLeads> mindtreeLeads = leadBL.DisplayDetailsOfLead();

            foreach (var lead in mindtreeLeads)
            {
                mindtreeLeadModels.Add(AssignLead(lead));
            }
            return mindtreeLeadModels;
        }
        
        public List<CategoryModel> GetDetailsOfCategory()
        {
            List<CategoryModel> categoryModels = new List<CategoryModel>();
            List<Category> categories = leadBL.GetDetailsOfCategory();

            foreach (var category in categories)
            {
                categoryModels.Add(AssignCategory(category));
            }
            return categoryModels;
        }
        public CategoryModel AssignCategory(Category category)
        {
            CategoryModel categoryModel = new CategoryModel()
            {
                CategoryId = category.CategoryId,
                CategoryName = category.CategoryName
            };
            return categoryModel;
        }
        public MindtreeLeadsModel AssignLead(MindtreeLeads lead)
        {
            MindtreeLeadsModel mindtreeLeadModel = new MindtreeLeadsModel()
            {
                LeadId = lead.LeadId,
                LeadName = lead.LeadName,
                ContactNumber = lead.ContactNumber,
                CategoryId = lead.CategoryId
            };
            return mindtreeLeadModel;
        }
        public int UpdateLeadDetail(MindtreeLeadsModel mindtreeLeadModel, CategoryModel categoryModel)
        {
            MindtreeLeads mindtreeLead = new MindtreeLeads()
            {
                LeadId = mindtreeLeadModel.LeadId,
                LeadName = mindtreeLeadModel.LeadName,
                ContactNumber = mindtreeLeadModel.ContactNumber
            };
            Category category = new Category()
            {
                CategoryId = categoryModel.CategoryId
            };
            return leadBL.UpdateLeadDetail(mindtreeLead, category);
        }
        
    }
}