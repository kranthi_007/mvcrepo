﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kalinga.PresentationLayer.Models
{
    public class MindtreeLeadsModel
    {
        [DisplayName("Lead Id")]
        public int LeadId { get; set; }

        [DisplayName("Lead Name")]
        [Required(ErrorMessage = "Enter the Lead Name")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Invalid Lead Name")]
        public string LeadName { get; set; }

        [DisplayName("Contact Number")]
        [Required(ErrorMessage = "Enter the Lead Contact")]
        [RegularExpression("^[0-9]{10}$", ErrorMessage = "Invalid Phone Number ")]
        public string ContactNumber { get; set; }

        [DisplayName("Category Id")]
        [Required(ErrorMessage = "Enter the CategoryId")]
        public int CategoryId { get; set; }
    }
}