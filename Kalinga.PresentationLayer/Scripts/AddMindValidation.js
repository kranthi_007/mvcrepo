﻿function ValidateCampusMindForm()
{
    var MindName = $("#MindName").val();
    var ContactNumber = $("#ContactNumber").val();
    if (MindName == "")
    {
        alert("Mind Name Column Cannot Be Empty");
    }
    if (ContactNumber == "" )
    {
        alert("Contact Number Column Cannot Be Empty");
    }
    if (ContactNumber != RegExp("[0-9]{10}"))
    {
        alert("Contact Number Column Should not Exceed 10 digits");
    }
}